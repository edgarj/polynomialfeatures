#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct 24 09:55:47 2020

@author: johnedgar
"""

import numbers
import numpy as np

class Desirability:
    '''
    Defines a single desirability function d(Y) for a given optimization 
    objective.
    
    Ref: https://www.itl.nist.gov/div898/handbook/pri/section5/pri5322.htm

    Parameters
    ----------
    objective : str
        One of 'max', 'min', 'target', or None. If None, a constant 1 is 
        returned for all values provided. This is to allow optimization over
        a subset of a MultiRSM object.
    y_lower : float
        The minimum value of Y. If objective == 'max', every Y < y_lower
        returns 0. If objective == 'min', every Y < y_lower returns 1.
    y_upper : float
        The maximum value of Y. If objective == 'max', every Y > y_upper
        returns 1. If objective == 'min', every Y > y_lower returns 0.
    y_target : float, None, optional
        A target value for optimization when objective == 'target'.
        The default is None.
    power : float, optional
        A power term applied to the desirability function. When power = 1.0
        the function is piecewise linear. The default is 1.0.
    name : str, None, optional
        A name for the function. Strongly recommended when using
        :class:`desirability.Desirability` within
        :class:`desirability.OverallDesirability` in order to keep track
        of multiple functions. The default is None.

    '''
    def __init__(self, objective, y_lower, y_upper, y_target = None, power = 1.0, name = None):
        assert objective in ['min', 'max', 'target', 'none',None]
        
        if objective == 'target' and y_target is None:
            raise ValueError('Must provide value to y_target when objective = "target".')
        elif objective in ['min','max']:
            y_target = None
        elif objective is None:
            y_lower = None
            y_upper = None
            y_target = None
        
        if y_upper is not None and y_lower is not None:
            assert y_upper > y_lower
        if y_target is not None:
            assert y_target > y_lower and y_target < y_upper
        
        self.objective = objective
        self.y_lower = y_lower
        self.y_upper = y_upper
        self.y_target = y_target
        self.pow = power
        self.name = name
      
        
    def __call__(self, Y):
        '''
        Calls the desirability function

        Parameters
        ----------
        Y : np.ndarray
            An array of shape (n,1) where n is the number of sample points being tested

        Returns
        -------
        np.ndarray
            Array of shape (n,1) where n is the number of y values (sample points)
            provided to this function and each row is the desirability
            for the corresponding row in Y.

        '''
        Y = np.asarray(Y)
        
        if self.objective == 'max':
            d = np.zeros(Y.shape)
            d += (Y <= self.y_lower) * 0.0
            d += ((Y > self.y_lower) & (Y < self.y_upper)) * self._maximize(Y)
            d += (Y >= self.y_upper) * 1.0
            return d

        elif self.objective == 'min':
            d = np.zeros(Y.shape)
            d += (Y <= self.y_lower) * 1.0
            d += ((Y > self.y_lower) & (Y < self.y_upper)) * self._minimize(Y)
            d += (Y >= self.y_upper) * 0.0
            return d
            
        elif self.objective == 'target':
            d = np.zeros(Y.shape)
            d += (Y <= self.y_lower) * 0.0
            d += ((Y > self.y_lower) & (Y <= self.y_target)) * self._targetl(Y)
            d += ((Y > self.y_target) & (Y < self.y_upper)) * self._targeth(Y)
            d += (Y >= self.y_upper) * 0.0
            return d
        
        else:
            return np.ones(Y.shape)
            

        
    def _maximize(self, Y):
        return ((Y - self.y_lower)/(self.y_upper - self.y_lower))**self.pow
    
    def _minimize(self, Y):
        return ((Y - self.y_upper)/(self.y_lower - self.y_upper))**self.pow
    
    def _targetl(self, Y):
        return ((Y - self.y_lower)/(self.y_target - self.y_lower))**self.pow
    
    def _targeth(self, Y):
        return ((Y - self.y_upper)/(self.y_target - self.y_upper))**self.pow
    
    def sweep(self):
        '''
        Sweeps through the range provided by [y_lower, y_upper] and calculates
        the defined desirability. Useful for plotting by not much else.
        
        Returns
        -------
        Y : np.ndarray
            The values swept
        d : np.ndarray
            The desirability for the range in Y
            
        '''
        rng = (self.y_upper - self.y_lower) * 0.1
        Y = np.linspace(self.y_lower - rng, self.y_upper + rng, 100)
        return Y, self.__call__(Y)
    
class OverallDesirability:
    '''
    Creates an object to calculate the overall desirability :math:`D` from
    multiple desirabilities :math:`d_i` using the geometric mean
    :math:`D = (d_1*d_2 *...* d_n)^{1/n}`. Individual desirabilities are
    :class:`desirability.Desirability` objects.

    Parameters
    ----------
    objective : list, tuple, str
        A list of objectives for each function being optimized. If the same
        objective (ie. 'max') is being applied to all functions, a string
        can be provided here and a value provided to n_objectives.
    y_lower : list, tuple, numeric
        A list of lower bounds for each function being optimized. Refer to
        :class:`desirability.Desirability` for details. Can be a single
        value applied to all functions when n_objectives is not None.
    y_upper : list, tuple, numeric
        A list of upper bounds for each function being optimized. Refer to
        :class:`desirability.Desirability` for details. Can be a single
        value applied to all functions when n_objectives is not None.
    y_target : list, tuple, numeric, NoneType, optional
        A list of targets for each function being optimized. Refer to
        :class:`desirability.Desirability` for details. If mixed with 'min'
        or 'max' objectives, provide as [None, None, y_target, None,...].
        The default is None.
    n_objective : int, NoneType, optional
        The number of objectives to optimize. Ignored if objectives is a
        list. The default is None.
    power : list, tuple, numeric, optional
        Power applied to desirability functions. Refer to
        :class:`desirability.Desirability` for details. The default is 1.0.
    names : list, tuple, NoneType, optional
        A list of names for each desirability function created.
        The default is None.

    '''
    def __init__(self, objective, y_lower, y_upper, y_target = None, n_objective = None, power = 1.0, names = None):
        # Build objective list
        assert type(n_objective) is int or n_objective is None
        if type(objective) is str and n_objective is None:
            raise ValueError('Must provide objective as a list or provide a value for n_objective')
        elif type(objective) is str:
            objective = [objective for _ in range(n_objective)]
        elif isinstance(objective, (list, tuple)):
            assert all(type(elm) is str or elm is None for elm in objective)
            objective = objective
            n_objective = len(objective)
        else:
            raise TypeError('Provide objective as a str or list of str')
         
        # Build y_lower list
        if isinstance(y_lower, numbers.Number):
            y_lower = [y_lower for _ in range(n_objective)]
        elif isinstance(y_lower, (list, tuple)):
            assert all(isinstance(elm, numbers.Number) for elm in y_lower)
            assert len(y_lower) == n_objective
        else:
            raise TypeError('Must provide y_lower as a number or list of numbers')
          
        # Build y_upper list
        if isinstance(y_upper, numbers.Number):
            y_upper = [y_upper for _ in range(n_objective)]
        elif isinstance(y_upper, (list, tuple)):
            assert all(isinstance(elm, numbers.Number) for elm in y_upper)
            assert len(y_upper) == n_objective
        else:
            raise TypeError('Must provide y_upper as a number or list of numbers')
         
        # Build y_target list
        if y_target is None:
            assert 'target' not in objective
            y_target = [y_target for _ in range(n_objective)]
        elif isinstance(y_target, numbers.Number):
            y_target = [y_target for _ in range(n_objective)]
        elif isinstance(y_target, (list, tuple)):
            assert all(isinstance(elm, numbers.Number) for elm in y_target)
            assert len(y_target) == n_objective
        else:
            raise TypeError('Must provide y_target as a None, a number, or a list of mixed None/numbers')
          
        # Build power list
        if isinstance(power, numbers.Number):
            power = [power for _ in range(n_objective)]
        elif isinstance(power, (list, tuple)):
            assert all(isinstance(elm, numbers.Number) for elm in power)
            assert len(power) == n_objective
        else:
            raise TypeError('Must provide power as a number or list of numbers')    
         
        # Build names list
        if names is None:
            names = [names for _ in range(n_objective)]
        elif isinstance(names, (list, tuple)):
            assert all(isinstance(elm, str) for elm in names)
            assert len(names) == n_objective
        else:
            raise TypeError('Must provide names as a None or a list of str')
           
        self.n_objective = n_objective
        self.desirability = [Desirability(obj, yl, yu, yt, pwr, nm)
                             for obj, yl, yu, yt, pwr, nm
                             in zip(objective, y_lower, y_upper, y_target, power, names)]
            
    def __call__(self, Y):
        '''
        Calls multiple desirability functions then calculates the geometric mean
        of all desirabilities to determine the overall desirability.

        Parameters
        ----------
        Y : np.ndarray
            An array of shape (n,r) where r is the number of objectives being
            optimized and n is the number of sample points being tested

        Returns
        -------
        nd.array
            Array of shape (n,1) where n is the number of y values (sample points)
            provided to this function and each row is an overall desirability
            for the corresponding row in Y.

        '''
        assert isinstance(Y,np.ndarray)
        assert Y.shape[1] == self.n_objective, 'The number of columns in Y does not match the number of objectives'
        
        # Send each column to its desirability function
        d = np.vstack([func(Y[:,idx]) for func, idx in zip(self.desirability, range(Y.shape[1]))])
        # Calculate overall desirability for each row
        return np.prod(d.T, axis = 1, keepdims = True)**(1/self.n_objective)
        
        
        
        
            
        
        
        