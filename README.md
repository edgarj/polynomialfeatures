# Polynomial Features
A small library for building polynomial functions from regression coefficients. Uses `sklearn.preprocessing.PolynomialFeatures` (hence the name).

## Dependencies
Built with Python 3.7. Other versions of Python 3.X are expected to work but are untested.

- numpy
- scipy
- pandas
- scikit_learn

## Installation
Dependencies can be installed by navigating to the directory and running `pip install -r requirements.txt`. It is recommended to install into a new Conda environment. For testing purposes, clone into a git repository as a submodule.

## Usage
The library is split into three modules: `rsmprofiler`, `rsmoptimizer`, and `desirability`. The `rsmprofiler` module containts the `RSMProfiler` class object which loads regression coefficients from a .csv file containing the columns "Terms" and "Estimate" and  and builds a polynomial regression model to the order specified in the .csv. `RSMProfiler.sweep()` sweeps single factors from low to high (as specified) while holding other factors constant. `RSMProfiler.sweep_2FI()` does the same thing but for two factors at a time. `RSMProfiler` also contains methods to convert between scaled and actual factor levels (and vice versa). `MultiRSM` is a wrapper that can hold multiple `RSMProfiler` objects and provides the same interface but for multiple regression models simultaneously. 

`desirability.OverallDesirability` implements desirability functions that can be used for multi-objective optimization of a `rsmprofiler.MultiRSM` object. This class is instantiated with a number of objective ("min","max", or "target") – one for each model contained in the `MultiRSM`.

Finally, `rsmoptimize.OptObjects` is a class for holding a `rsmprofiler.MultiRSM` instance and its corresponding `desirability.OverallDesirability` instance. The purpose of this class is to call `MultiRSM` with some parameter X and then use the returned Y to calculate the desirability score using `OverallDesirability`. The `rsmoptimize.OptObjects` instance can be provided to `rsmoptimize.basinhopping` which will perform optimization to maximize the desirability score.
